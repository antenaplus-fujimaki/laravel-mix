# Laravel-mix

Sourcemapは開発時のみインラインで吐き出すようにしています。  

Laravel-mixの詳しい使い方はこちら。  
https://readouble.com/laravel/5.7/ja/mix.html

## 使い方

yarn run dev：ファイル修正時に使用  
yarn run watch：リアルタイムでファイル変更を監視  
yarn run prod：本番環境用ファイル生成（cssとjsからソースマップが削除されて、minifyされる）

## 機能一覧
- Webpack
- Sass
- Autoprefixer　ベンダープレフィックスを自動でつけてくれる
- Sourcemap　JSのSourcemapも出力されます
- Browsersync　プレビュー画面
- css-mqpacker　CSSをメディアクエリーごとに並べ直してくれる
- css-declaration-sorter　CSSを最適化してくれる。オーダーはsmacss。
- Pug　テンプレートエンジン
- Babel　JS最適化